//
//  FZMainViewController.m
//  FZSplitViewTest
//
//  Created by Fan Zhang on 6/6/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import "FZMainViewController.h"
#import "FZLeftViewController.h"
#import "FZRightViewController.h"

@interface FZMainViewController ()

@end

@implementation FZMainViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"embedView"]) {
        UISplitViewController *splitViewController = (UISplitViewController *)[segue destinationViewController];
        UINavigationController *leftNavController = [splitViewController.viewControllers objectAtIndex:0];
        FZLeftViewController *leftViewController = (FZLeftViewController *)[leftNavController topViewController];
        FZRightViewController *rightViewController = [splitViewController.viewControllers lastObject];
        NSString *displayStr = [leftViewController.displayStrs firstObject];
        [rightViewController setDisplayStr:displayStr];
        splitViewController.delegate = rightViewController;
        
//        splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;//default
//        [self setOverrideTraitCollection:[UITraitCollection traitCollectionWithHorizontalSizeClass:UIUserInterfaceSizeClassRegular] forChildViewController:splitViewController];
//        splitViewController.preferredPrimaryColumnWidthFraction = .5;//control width
        
        self.splitViewController = splitViewController;
        
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    
    if (size.width>320) {
        [self setOverrideTraitCollection:[UITraitCollection traitCollectionWithHorizontalSizeClass:UIUserInterfaceSizeClassRegular] forChildViewController:self.splitViewController];
    }else{
        [self setOverrideTraitCollection:[UITraitCollection traitCollectionWithHorizontalSizeClass:UIUserInterfaceSizeClassCompact] forChildViewController:self.splitViewController];
    }
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
}

@end
